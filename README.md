# vue

#### 介绍
项目作为新技能调试学习平台

#### 安装说明
1. git clone https://gitee.com/dingguoqing/vue.git
2. cd vue
3. npm install
4. npm run dev


#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
